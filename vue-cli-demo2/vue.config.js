module.exports = {
    devServer: {
        port: 8081,
        open: true,
        https: false,
        host: '127.0.0.1'
    },

    lintOnSave: false,
    outputDir: 'dist2',
    assetsDir: 'assets',
    indexPath: 'out/index.html',
    productionSourceMap: false
}