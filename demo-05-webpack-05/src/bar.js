// Node 导出默认成员
// module.exports = function(){
//     console.log('我是 bar Node')
// }

// ES6 导出默认成员
// export default function(){
//     console.log('我是 bar ES6')
// }

// export default {
//     "name": "菜园子1122"
// }
// export default "菜园子"

// ES6 导出非默认成员

export const x = 'xxx'
export const y = 'yyy'

export function add(a,b){
    return a+b
}