// Node 导入模块
// var bar = require('./bar')
// bar()

// ES6 导入
// 默认加载的是 export default 成员
// import bar from './bar'
// console.log(bar)

// import {x,y,add} from './bar'
// console.log(x,y,add(10,30))

// import {x,y} from './bar'
// console.log(x,y)

import * as bar from './bar'
console.log(bar)
console.log(bar.x,bar.y,bar.add(1,3))