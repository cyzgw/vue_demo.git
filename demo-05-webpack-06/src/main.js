import App from './App.vue'

// 第一种方法
// import Vue from 'vue/dist/vue.js'
// 第二种方法
import Vue from 'vue'

// new Vue({
//     el: '#app',
//     template: '<App />',
//     components: {App}
// })
new Vue({
    el: '#app',
    render: h => h(App)
})