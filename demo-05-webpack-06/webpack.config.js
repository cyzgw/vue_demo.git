// 引用Node.js 中的 path 路径，处理文件路径的小工具
const path = require('path')

// 引入插件
const HtmlWebpackPlugin = require('html-webpack-plugin')

const VueLoaderPlugin = require('vue-loader/lib/plugin')

// 1引入webpack
const webpack = require('webpack')

// 1.导出一个webpack具有特殊属性配置的对象
module.exports = {
    // 入口
    entry: './src/main.js', //入口模块文件路径
    //出口是对象
    output: {
        // output 必须是一个绝对路径,__dirname 是当前js的绝对路径
        path: path.join(__dirname, './dist'), // 打包的结果文件存储目录
        filename: 'bundle.js'//打包的结果文件名
        
    },
    devServer: {
        contentBase: './dist',
        // 开启模块热加载
        hot: true
    },
    // 配置插件
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        new VueLoaderPlugin(),
        // 3模块热替换
        new webpack.HotModuleReplacementPlugin()
    ],
    mode: 'none',
    module: {
        rules:[
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.m?js$/,
                exclude: /(node_moudules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    // resolve: {
    //     alias: {
    //         'vue$': 'vue/dist/vue.js'
    //     }
    // }
}