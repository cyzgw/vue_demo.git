;(function(){
    window.router = new VueRouter({
        linkActiveClass: 'active',
        routes: [
            {path: '/', component: AppHome},
            {
                path: '/news',
                component: News,
                children: [
                    // 当匹配到/news/sport请求时
                    // 组件Sport会被渲染在News组件中的<router-view>中
                    {
                        path: '/news/sport',
                        component: Sport,
                        children: [
                            // 配置详情请求路径
                            {
                                path: '/news/sport/detail/:id',//id路径变量占位符
                                component: SportDetail
                            }
                            
                        ]
                    },
                    // 简写方式，等价于/news/tech路径，注意前面没有/，有/就是根目录了
                    {
                        path: 'tech',
                        component: Tech,
                        children: [
                            {
                                path: '/news/tech/detail/:id',
                                component: TechDetail
                            }
                        ]
                    },
                    // 点击新闻管理默认选项中 新闻，
                    // 就是/news后面没有子路径时，redirect 重定向到 体育
                    {
                        path: '',
                        redirect: '/news/sport'
                    }
                ]
            },
            {path: '/about', component: About}
        ]
    })
})()