;(function(){
    const template = `<div>
        <ul >
            <li v-for="(tech,index) in techArr" :key="tech.id">
                <span>{{tech.title}} </span>
                <button class="btn  btn-default btn-xs" @click="pushTech(tech.id)">查看(Push)</button>&nbsp;
                <button class="btn btn-default btn-xs" @click="replaceTech(tech.id)">查看(replace)</button>
            </li>
        </ul>
        <button @click="$router.go(-1)">后退</button> 
        <button @click="$router.go(1)">前进</button> 
        <!--详情-->
        <router-view></router-view>
    </div>`
    window.Tech = {
        data() {
            return {
                techArr: []
            }
        },
        created() {
            this.getTechArr()
        },
        methods: {
            getTechArr(){
                axios.get("http://127.0.0.1:5500/vue-03-router/bootstrap-ajax-router/data/tech.json").then(response => {
                    console.log(response.data,this)
                    this.techArr = response.data
                }).catch(error => {
                    alert(error.message)
                })
            },
            pushTech(id){
                this.$router.push( `/news/tech/detail/${id}`)
            },
            replaceTech(id){
                this.$router.replace( `/news/tech/detail/${id}`)
            }
        },
        template
    }
})()