;(function(){
    const template = `<div>
        <ul>
            <li v-for="(sport,index) in sportArr" :key="sport.id">
                <!-- <a href="#">{{sport.title}}</a> -->
                <router-link :to="'/news/sport/detail/'+sport.id">
                    {{sport.title}}
                </router-link>
            </li>
            
        </ul>
        <!--详情-->
        <router-view></router-view>
    </div>  `
    window.Sport =  {
        data() {
            return {
                sportArr: []
            }
        },
        created() {
            this.getSportArr()
        },
        methods: {
            getSportArr(){
                axios.get('http://127.0.0.1:5500/vue-03-router/bootstrap-ajax-router/data/sport.json').then(response => {
                    console.log(123)
                    console.log(response.data,this)
                    this.sportArr = response.data
                }).catch(error => {
                    alert(error.message)
                })
            }
        },
        template
    }
})()