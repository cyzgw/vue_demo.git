;(function(){
    const template = `
    <div class="jumbotron">
        <h2>{{techDetail.title}}</h2>
        <p>{{techDetail.content}}</p>
    </div>
    `
    window.TechDetail = {
        template,
        data() {
            return {
                techDetail: {}
            }
        },
        created() {
            this.getTechById()
        },
        methods: {
            getTechById(){
                const id = this.$route.params.id - 0
                // 2. 获取所有的体育新闻
                axios.get('http://127.0.0.1:5500/vue-03-router/bootstrap-ajax-router/data/tech.json')
                .then(response => {
                    const techArr = response.data
                  
                    this.techDetail = techArr.find(detail => {
                        // this 如果要代表 当前组件对象，则 回调函数要使用箭头函数
                        return detail.id === id
                    })
                })
                .catch(error => {
                    alert(error.message)
                })
            }
        },
        watch: {
            '$route': function(){
                this.getTechById()
            }
        }
    }
})()