// 引用Node.js 中的 path 路径，处理文件路径的小工具
const path = require('path')

// 1.导出一个webpack具有特殊属性配置的对象
module.exports = {
    // 入口
    entry: './src/main.js', //入口模块文件路径
    //出口是对象
    output: {
        // output 必须是一个绝对路径,__dirname 是当前js的绝对路径
        path: path.join(__dirname, './dist'), // 打包的结果文件存储目录
        filename: 'bundle.js'//打包的结果文件名
        
    },
    mode: 'none'
}